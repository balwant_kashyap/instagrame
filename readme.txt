=== Spice Instagram ===

Contributors: 		spicethemes
Tags: 				Instagram, Instagram feed, Instagram photos, Instagram gallery
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		5.9
Stable tag: 		0.1
License: 			GPLv2 or later
License URI: 		https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

This plugin allows you to display beautifully clean, customizable, and responsive Instagram feeds with multiple options, It is responsive ready so it will work perfectly on different devices like mobile and iPad. The important feature of the plugin is providing the typography and color settings from where you can change the typography and color of the content. 

<h3>Key Features</h3>

* Typography setting
* Color setting
* Section Padding
* Column Layout
* Image Overlay


== Changelog ==

@Version 0.1
* Initial Release.

======= External Resources =======

Magnific-Popup:
Copyright (c) 2014-2016 Dmitry Semenov
License: MIT License
Source: https://github.com/dimsemenov/Magnific-Popup

Jquery UI:
Copyright: (c) jQuery Foundation
License: MIT License
Source: http://jqueryui.com