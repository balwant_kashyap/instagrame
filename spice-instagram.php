<?php
/*
* Plugin Name:			Spice Instagram 
* Plugin URI:  			
* Description: 			This plugin allows you to display beautifully clean, customizable, and responsive Instagram feeds with multiple options, It is responsive ready so it will work perfectly on different devices like mobile and iPad. The important feature of the plugin is providing the typography and color settings from where you can change the typography and color of the content. 
* Version:     			0.1
* Requires at least: 	5.3
* Requires PHP: 		5.2
* Tested up to: 		5.9
* Author:      			Spicethemes
* Author URI:  			https://spicethemes.com
* License: 				GPLv2 or later
* License URI: 			https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 			spice-instagram
* Domain Path:  		/languages
*/

// Exit if accessed directly
if( ! defined('ABSPATH'))
{
	die('Do not open this file directly.');
}

/**
 * Main Spice_Instagram Class
 * @class Spice_Instagram
 * @since 0.1
 * @package Spice_Instagram
*/

final Class Spice_Instagram {

	/**
	 * The version number.
	 *
	 * @var     string
	 * @access  public
	 * @since   0.1
	 */
	public $version;


	/**
	 * Constructor function.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function __construct() {
		$this->plugin_url  = plugin_dir_url( __FILE__ );
		$this->plugin_path = plugin_dir_path( __FILE__ );
		$this->version     = '0.1';

		define( 'SPICE_INSTAGRAM_URL', $this->plugin_url );
		define( 'SPICE_INSTAGRAM_PATH', $this->plugin_path );
		define( 'SPICE_INSTAGRAM_VERSION', $this->version );

		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		//Output file to Instagram Feed
		require_once SPICE_INSTAGRAM_PATH . '/include/view/shortcode.php';

		//Register spice_instagram CPT
		require_once SPICE_INSTAGRAM_PATH . '/include/admin/spice-instagram-cpt.php';

		//Font file
		require_once SPICE_INSTAGRAM_PATH . '/include/admin/spice-instagram-fonts.php';
	}

	/**
	 * Load the localisation file.
	 *
	 * @access  public
	 * @since   0.1
	 * @return  void
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain( 'spice-instagram' , false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}


}

new Spice_Instagram;