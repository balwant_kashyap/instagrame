<?php
// Exit if accessed directly
if (!defined('ABSPATH')) 
{
    die('Do not open this file directly.');
}

add_shortcode('spice_instagram', 'spice_instagram_shortcode');
if ( !function_exists( 'spice_instagram_shortcode' ) ) {
    function spice_instagram_shortcode($id)
    {
        ob_start();
        $spice_instagram_post_id        = !isset($id['id']) ? $spice_instagram_shortcodes_cpt_id='' : $spice_instagram_shortcodes_cpt_id=$id['id'];
        $spice_instagram_padding_top    = get_post_meta($spice_instagram_post_id,'spice_instagram_padding_top', true );
        $spice_instagram_padding_right  = get_post_meta($spice_instagram_post_id,'spice_instagram_padding_right', true );
        $spice_instagram_padding_bottom = get_post_meta($spice_instagram_post_id,'spice_instagram_padding_bottom', true );
        $spice_instagram_padding_left   = get_post_meta($spice_instagram_post_id,'spice_instagram_padding_left', true );
        $spice_instagram_title          = get_post_meta($spice_instagram_post_id,'spice_instagram_title', true );
        $spice_instagram_image          = get_post_meta($spice_instagram_post_id,'spice_instagram_image', true );
        $spice_instagram_text           = get_post_meta($spice_instagram_post_id,'spice_instagram_text', true );
        $spice_instagram_overlay        = get_post_meta($spice_instagram_post_id,'spice_instagram_overlay', true );
        $spice_instagram_title_clr      = get_post_meta($spice_instagram_post_id,'spice_instagram_title_clr', true );
        $spice_instagram_overlay_clr    = get_post_meta($spice_instagram_post_id,'spice_instagram_overlay_clr', true );
        $spice_instagram_title_ff       = get_post_meta($spice_instagram_post_id,'spice_instagram_title_ff', true );
        $spice_instagram_title_fs       = get_post_meta($spice_instagram_post_id,'spice_instagram_title_fs', true );
        $spice_instagram_title_lheight  = get_post_meta($spice_instagram_post_id,'spice_instagram_title_lheight', true );
        $spice_instagram_title_fw       = get_post_meta($spice_instagram_post_id,'spice_instagram_title_fw', true );
        $spice_instagram_title_fstyle   = get_post_meta($spice_instagram_post_id,'spice_instagram_title_fstyle', true );
        $spice_instagram_title_trans    = get_post_meta($spice_instagram_post_id,'spice_instagram_title_trans', true );
        $spice_instagram_title_align    = get_post_meta($spice_instagram_post_id,'spice_instagram_title_align', true );
        $spice_instagram_col            = get_post_meta($spice_instagram_post_id,'spice_instagram_col', true );?>
        <section class="spice-instagram-wrap" id="spice-instagram-section<?php echo $spice_instagram_post_id;?>">
            <?php if($spice_instagram_title != 'spice_instagram_blank'):?><p><?php echo esc_attr($spice_instagram_title);?></p><?php endif;?>
            <div class="spice-instagram-row spice-instagram-gallery<?php echo esc_attr($spice_instagram_post_id);?>" id="instafeed-container<?php echo esc_attr($spice_instagram_post_id);?>"></div>
            <?php if(!empty(get_option('spice_instagram_text'))):?>
            <script type="text/javascript">
            jQuery(document).ready(function(){
                var userFeed = new Instafeed({
                        get: 'user',
                        limit:<?php echo intval($spice_instagram_image);?>,
                        target: "instafeed-container<?php echo intval($spice_instagram_post_id);?>",
                        resolution: 'low_resolution',
                        sortBy: 'most-recent',
                        template: '<div class="item-'+<?php echo esc_attr($spice_instagram_col);?>+'column"><div class="portDetail"><figure class="spice-instagram-thumbnail'+<?php echo $spice_instagram_post_id;?>+'"><a href="{{image}}"><img title="{{caption}}" src="{{image}}" /></a></figure></div></div>',
                        accessToken: '<?php echo esc_attr(get_option('spice_instagram_text'));?>'
                    });
                    userFeed.run();

                   // This will create a single gallery from all elements that have class "gallery-item"
                  jQuery('.spice-instagram-gallery<?php echo intval($spice_instagram_post_id);?>').magnificPopup({
                  delegate: 'a', // child items selector, by clicking on it popup will open  
                  type: 'image',
                  gallery:{
                    enabled:true
                  }
                });

            });
           </script>
       <?php endif;?>
        </section>
        <style type="text/css">
            body #spice-instagram-section<?php echo esc_attr($spice_instagram_post_id);?>
            {
                padding-top: <?php echo intval($spice_instagram_padding_top);?>px;
                padding-right: <?php echo intval($spice_instagram_padding_right);?>px;
                padding-bottom: <?php echo intval($spice_instagram_padding_bottom);?>px;
                padding-left: <?php echo intval($spice_instagram_padding_left);?>px;
            }
            body .spice-instagram-thumbnail<?php echo esc_attr($spice_instagram_post_id);?> a:hover img {
                opacity: <?php echo esc_attr($spice_instagram_overlay);?>;
            }
            body #spice-instagram-section<?php echo esc_attr($spice_instagram_post_id);?> p
            {
                color:  <?php echo esc_attr($spice_instagram_title_clr);?>;
            }
             body .spice-instagram-thumbnail<?php echo esc_attr($spice_instagram_post_id);?>
            {
                background-color: <?php echo esc_attr($spice_instagram_overlay_clr);?>;
            }
            #spice-instagram-section<?php echo esc_attr($spice_instagram_post_id);?> .spice-instagram-thumbnail<?php echo esc_attr($spice_instagram_post_id);?> {
                position: relative;
                overflow: hidden;
                width: 100%;
                text-align: left;
                border-radius: 0;
                margin-bottom: 0;
                margin-left: 0;
                margin-top: 0;
            }
            body #spice-instagram-section<?php echo esc_attr($spice_instagram_post_id);?> p
            {
                text-align: <?php echo esc_attr($spice_instagram_title_align);?>;
                font-family: '<?php echo esc_attr($spice_instagram_title_ff);?>';
                font-size: <?php echo intval($spice_instagram_title_fs);?>px;
                line-height: <?php echo intval($spice_instagram_title_lheight);?>px;
                font-weight: <?php echo intval($spice_instagram_title_fw);?>;
                font-style: <?php echo esc_attr($spice_instagram_title_fstyle);?>;
                text-transform: <?php echo esc_attr($spice_instagram_title_trans);?>;
            }
        </style>
        <?php
        return ob_get_clean();
    }
}