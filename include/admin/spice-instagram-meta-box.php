<?php
//General
$spice_instagram_padding_top    = get_post_meta( get_the_ID(), 'spice_instagram_padding_top', true );
$spice_instagram_padding_top    = empty($spice_instagram_padding_top) ? 0 : $spice_instagram_padding_top;
$spice_instagram_padding_right  = get_post_meta( get_the_ID(), 'spice_instagram_padding_right', true );
$spice_instagram_padding_right  = empty($spice_instagram_padding_right) ? 0 : $spice_instagram_padding_right;
$spice_instagram_padding_bottom = get_post_meta( get_the_ID(), 'spice_instagram_padding_bottom', true );
$spice_instagram_padding_bottom = empty($spice_instagram_padding_bottom) ? 0 : $spice_instagram_padding_bottom;
$spice_instagram_padding_left   = get_post_meta( get_the_ID(), 'spice_instagram_padding_left', true );
$spice_instagram_padding_left   = empty($spice_instagram_padding_left) ? 0 : $spice_instagram_padding_left;
$spice_instagram_image          = get_post_meta( get_the_ID(), 'spice_instagram_image', true );
$spice_instagram_image          = empty($spice_instagram_image) ? 4 : $spice_instagram_image;
$spice_instagram_col            = get_post_meta( get_the_ID(), 'spice_instagram_col', true );
$spice_instagram_col            = empty($spice_instagram_col) ? 2 : $spice_instagram_col;
$spice_instagram_overlay        = get_post_meta( get_the_ID(), 'spice_instagram_overlay', true );
$spice_instagram_overlay        = empty($spice_instagram_overlay) ? 0.7 : $spice_instagram_overlay;
$spice_instagram_title          = get_post_meta( get_the_ID(), 'spice_instagram_title', true );
$spice_instagram_title_align    = get_post_meta( get_the_ID(), 'spice_instagram_title_align', true );

//Typography
$spice_instagram_title_ff       = get_post_meta( get_the_ID(), 'spice_instagram_title_ff', true );
$spice_instagram_title_fs       = get_post_meta( get_the_ID(), 'spice_instagram_title_fs', true );
$spice_instagram_title_lheight  = get_post_meta( get_the_ID(), 'spice_instagram_title_lheight', true );
$spice_instagram_title_fw       = get_post_meta( get_the_ID(), 'spice_instagram_title_fw', true );
$spice_instagram_title_fstyle   = get_post_meta( get_the_ID(), 'spice_instagram_title_fstyle', true );
$spice_instagram_title_trans    = get_post_meta( get_the_ID(), 'spice_instagram_title_trans', true );

//Color
$spice_instagram_title_clr      = get_post_meta( get_the_ID(), 'spice_instagram_title_clr', true );
$spice_instagram_overlay_clr    = get_post_meta( get_the_ID(), 'spice_instagram_overlay_clr', true );

$spice_instagram_font_size      = array();
    for($i=10; $i<=100; $i++) {
      $spice_instagram_font_size[$i] = $i;
    }
$spice_instagram_line_height    = array();
    for($i=10; $i<=100; $i++) {
        $spice_instagram_line_height[$i] = $i;
    }
$spice_instagram_font_family = spice_instagram_typo_fonts();?>
<div id="tabs">
  <ul>
    <li><a href="#tabs-1"><?php echo esc_html__('General','spice-instagram');?></a></li>
    <li><a href="#tabs-2"><?php echo esc_html__('Typography','spice-instagram');?></a></li>
    <li><a href="#tabs-3"><?php echo esc_html__('Color','spice-instagram');?></a></li>
  </ul>
  <div id="tabs-1">
    <table class="spice-instagram-tbl">
      <tr>
        <td class="spice-instagram-td-title"><?php echo esc_html__('Section Padding','spice-instagram');?>
        <span class="spice-instagram-td-span"><?php echo esc_html__('You can adjust section padding ( In Px )','spice-instagram');?></span></td>
        <td class="spice-instagram-td-attr">
          <label><?php echo esc_html__('Top:','spice-instagram');?> <input class="spice-instagram-td-padding" type="number" name="spice_instagram_padding_top" value="<?php echo esc_attr($spice_instagram_padding_top);?>" min="0" step="1"/></label> 
          <label><?php echo esc_html__('Left:','spice-instagram');?> <input class="spice-instagram-td-padding" type="number" name="spice_instagram_padding_right" value="<?php echo esc_attr($spice_instagram_padding_right);?>" min="0" step="1"></label> 
          <label><?php echo esc_html__('Bottom:','spice-instagram');?> <input class="spice-instagram-td-padding" type="number" name="spice_instagram_padding_bottom" value="<?php echo esc_attr($spice_instagram_padding_bottom);?>" min="0" step="1"/></label> 
          <label><?php echo esc_html__('Right:','spice-instagram');?> <input class="spice-instagram-td-padding" type="number" name="spice_instagram_padding_left" value="<?php echo esc_attr($spice_instagram_padding_left);?>" min="0" step="1"/></label> 
        </td>
        </tr>

      <tr>
        <td class="spice-instagram-td-title"><?php echo esc_html__('Section Title','spice-instagram');?>
        <span class="spice-instagram-td-span"><?php echo esc_html__('Enter your custom text for section title.','spice-instagram');?></span></td>
        <td class="spice-instagram-td-attr">
          <input class="spice-instagram-td-attr-width" type="text" name="spice_instagram_title" value="<?php if (!empty($spice_instagram_title)) { if($spice_instagram_title=='spice_instagram_blank') { } else { echo esc_attr($spice_instagram_title); } }  else { echo esc_html__('Instagram Feed','spice-instagram');} ?>"></td>
      </tr>  

      <tr>
        <td class="spice-instagram-td-title"><?php echo esc_html__('Section Title Alignment','spice-instagram');?>
        <span class="spice-instagram-td-span"><?php echo esc_html__('Set section title as center/left/right.','spice-instagram');?></span></td>
        <td class="spice-instagram-td-attr">
          <select class="spice-instagram-td-attr-width" name="spice_instagram_title_align" style="display:block;">
            <option value="center" <?php if($spice_instagram_title_align == 'center') { echo "selected"; } ?>><?php echo esc_html__('Center','spice-instagram');?></option>
            <option value="left" <?php if($spice_instagram_title_align == 'left') { echo "selected"; } ?>><?php echo esc_html__('Left','spice-instagram');?></option>
            <option value="right" <?php if($spice_instagram_title_align == 'right') { echo "selected"; } ?>><?php echo esc_html__('Right','spice-instagram');?></option>
          </select>  
         </td>
      </tr> 
      
      <tr>
        <td class="spice-instagram-td-title"><?php echo esc_html__('Image','spice-instagram');?>
        <span class="spice-instagram-td-span"><?php echo esc_html__('Enter the number to display instagram feeds. (Default is 4 )','spice-instagram');?></span></td>
        <td class="spice-instagram-td-attr">
          <input class="spice-instagram-td-attr-width" type="number" name="spice_instagram_image" value="<?php echo esc_attr($spice_instagram_image);?>" min="1" step="1"/></td>
        </tr>

        <tr>
          <td class="spice-instagram-td-title"><?php echo esc_html__('Image Overlay','spice-instagram');?>
          <span class="spice-instagram-td-span"><?php echo esc_html__('Set image overlay. (Default is 0.7 )','spice-instagram');?></span></td>
          <td class="spice-instagram-td-attr">
            <input class="spice-instagram-td-attr-width" type="number" name="spice_instagram_overlay" value="<?php echo esc_attr($spice_instagram_overlay);?>" min="0.1" step="0.1" max="1"/></td>
        </tr>

        <tr>
          <td class="spice-instagram-td-title"><?php echo esc_html__('Column Layout','spice-instagram');?>
          <span class="spice-instagram-td-span"><?php echo esc_html__('Choose column layout (Default is 2 )','spice-instagram');?></span></td>
          <td class="spice-instagram-td-attr">
            <div id="spice_instagram_col_id" class="size-slider" ></div>
              <input type="text" class="slider-text" id="spice_instagram_col" name="spice_instagram_col"  readonly="readonly">
          </td>
        </tr>

    </table>
  </div>

  <!-- Tab 2 -->
  <div id="tabs-2">
    <table class="spice-instagram-tbl">
      <tr>
          <td class="spice-instagram-td-title"><?php echo esc_html__('Section Title','spice-instagram');?>
              <span class="spice-instagram-td-span"><?php echo esc_html__('Typography for section title.','spice-instagram');?></span>
          </td>
          <td class="spice-instagram-td-attr-30">
            <?php echo esc_html__('Font Family','spice-instagram');?>
            <select class="spice-instagram-font-select" name="spice_instagram_title_ff" style="display:block;">
              <?php
              foreach ($spice_instagram_font_family as $spice_instagram_key => $spice_instagram_value) { ?>
                <option value="<?php echo esc_attr($spice_instagram_key);?>" <?php if($spice_instagram_title_ff == $spice_instagram_key) { echo "selected"; } if(empty($spice_instagram_title_ff) && ($spice_instagram_key=='Poppins')) { echo "selected"; } ?> ><?php echo esc_html($spice_instagram_value);?></option>
              <?php }  ?>
            </select> 
          </td>
          <td class="spice-instagram-td-attr-30">
            <?php echo esc_html__('Font Size in px','spice-instagram');?>
            <select class="spice-instagram-font-select" name="spice_instagram_title_fs" style="display:block;">
              <?php
              foreach ($spice_instagram_font_size as $spice_instagram_font_size_key => $spice_instagram_font_size_value) { ?>
                <option value="<?php echo esc_attr($spice_instagram_font_size_key);?>" <?php if($spice_instagram_title_fs == $spice_instagram_font_size_key) { echo "selected"; } if( empty($spice_instagram_title_fs) && ($spice_instagram_font_size_key==24)) { echo "selected"; }  ?> ><?php echo esc_html($spice_instagram_font_size_value);?></option>
                <?php } ?>
            </select>
          </td>
      </tr>
      <tr>
        <td class="spice-instagram-td-title"></td>
        <td class="spice-instagram-td-attr-30">
          <?php echo esc_html__('Line Height in px','spice-instagram');?>
          <select class="spice-instagram-font-select" name="spice_instagram_title_lheight" style="display:block;">
            <?php
            foreach ($spice_instagram_line_height as $spice_instagram_line_height_key => $spice_instagram_line_height_value) { ?>
              <option value="<?php echo esc_attr($spice_instagram_line_height_key);?>" <?php if($spice_instagram_title_lheight == $spice_instagram_line_height_key) { echo "selected"; } if( empty($spice_instagram_title_lheight) && ($spice_instagram_line_height_key==36)) { echo "selected"; }  ?> ><?php echo esc_html($spice_instagram_line_height_value);?></option>
              
            <?php }  ?>
          </select>
        </td>
        <td class="spice-instagram-td-attr-30">
          <?php echo esc_html__('Font Weight','spice-instagram');?>
          <select class="spice-instagram-font-select" name="spice_instagram_title_fw" style="display:block;">
           <option value="100" <?php if($spice_instagram_title_fw == 100) { echo "selected"; } ?> ><?php echo esc_html__('100','spice-instagram');?></option>
            <option value="200" <?php if($spice_instagram_title_fw == 200) { echo "selected"; } ?> ><?php echo esc_html__('200','spice-instagram');?></option>
            <option value="300" <?php if($spice_instagram_title_fw == 300) { echo "selected"; } ?> ><?php echo esc_html__('300','spice-instagram');?></option>
            <option value="400" <?php if($spice_instagram_title_fw == 400) { echo "selected"; } ?> ><?php echo esc_html__('400','spice-instagram');?></option>
            <option value="500" <?php if($spice_instagram_title_fw == 500) { echo "selected"; } ?> ><?php echo esc_html__('500','spice-instagram');?></option>
            <option value="600" <?php if($spice_instagram_title_fw == 600) { echo "selected"; }  if(empty($spice_instagram_title_fw)) { echo "selected"; } ?> ><?php echo esc_html__('600','spice-instagram');?></option>
            <option value="700" <?php if($spice_instagram_title_fw == 700) { echo "selected"; } ?> ><?php echo esc_html__('700','spice-instagram');?></option>
            <option value="800" <?php if($spice_instagram_title_fw == 800) { echo "selected"; } ?> ><?php echo esc_html__('800','spice-instagram');?></option>
            <option value="900" <?php if($spice_instagram_title_fw == 900) { echo "selected"; } ?> ><?php echo esc_html__('Line Height in px','spice-instagram');?></option>
          </select>
        </td>
      </tr>
      <tr>
        <td class="spice-instagram-td-title"></td>
        <td class="spice-instagram-td-attr-30">
          <?php echo esc_html__('Font style','spice-instagram');?>
          <select class="spice-instagram-font-select" name="spice_instagram_title_fstyle" style="display:block;">
            <option value="normal" <?php if($spice_instagram_title_fstyle == 'normal') { echo "selected"; } ?>><?php echo esc_html__('Normal','spice-instagram');?></option>
            <option value="italic" <?php if($spice_instagram_title_fstyle == 'italic') { echo "selected"; } ?>><?php echo esc_html__('Italic','spice-instagram');?></option>
          </select>
        </td>
        <td class="spice-instagram-td-attr-30">
          <?php echo esc_html__('Text Transform','spice-instagram');?>
          <select class="spice-instagram-font-select" name="spice_instagram_title_trans" style="display:block;">
            <option value="initial" <?php if($spice_instagram_title_trans == 'initial') { echo "selected"; } ?>><?php echo esc_html__('Default','spice-instagram');?></option>
            <option value="capitalize" <?php if($spice_instagram_title_trans == 'capitalize') { echo "selected"; } ?>><?php echo esc_html__('Capitalize','spice-instagram');?></option>
            <option value="lowercase" <?php if($spice_instagram_title_trans == 'lowercase') { echo "selected"; } ?>><?php echo esc_html__('Lowercase','spice-instagram');?></option>
            <option value="Uppercase" <?php if($spice_instagram_title_trans == 'Uppercase') { echo "selected"; } ?>><?php echo esc_html__('Uppercase','spice-instagram');?></option>
          </select>
        </td>
      </tr>
    </table>

  </div>
   

   <div id="tabs-3">
    <table class="spice-instagram-tbl">
      <tr>
        <td class="spice-instagram-td-title"><?php echo esc_html__('Section Title Color','spice-instagram');?>
        <span class="spice-instagram-td-span"><?php echo esc_html__('You can change section title color.','spice-instagram');?></span></td>
        <td class="spice-instagram-td-attr">
          <input id="spice_instagram_title_clr" name="spice_instagram_title_clr" type="text" value="<?php if( empty($spice_instagram_title_clr) ) { echo '#000000'; } else {  echo esc_attr($spice_instagram_title_clr); } ?>" class="spice-instagram-color-field" data-default-color="#000000" />
         </td>
      </tr>
      <tr>
        <td class="spice-instagram-td-title"><?php echo esc_html__('Overlay Color','spice-instagram');?>
        <span class="spice-instagram-td-span"><?php echo esc_html__('You can change overlay color.','spice-instagram');?></span></td>
        <td class="spice-instagram-td-attr">
          <input id="spice_instagram_overlay_clr" name="spice_instagram_overlay_clr" type="text" value="<?php if( empty($spice_instagram_overlay_clr) ) { echo '#000000'; } else {  echo esc_attr($spice_instagram_overlay_clr); } ?>" class="spice-instagram-color-field" data-default-color="#000000" />
         </td>
      </tr>
    </table>
  </div>
</div>

<script>
  jQuery(function() {
    jQuery( "#spice_instagram_col_id" ).slider({
    orientation: "horizontal",
    range: "min",
    default:2,
    max: 4,
    min:1,
    slide: function( event, ui ) {
    jQuery( "#spice_instagram_col" ).val( ui.value );
      }
    });
    jQuery( "#spice_instagram_col_id" ).slider("value",<?php echo intval($spice_instagram_col);?> );
    jQuery( "#spice_instagram_col" ).val( jQuery( "#spice_instagram_col_id" ).slider( "value") );
});
</script>