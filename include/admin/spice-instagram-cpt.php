<?php
/**
* Register Post Type
*/
if ( ! class_exists( 'Spice_Instagram_Type' ) ) {

  class Spice_Instagram_Type
  {

    public function __construct()
    {
      //Register spice_instagram CPT
      add_action( 'init', array( $this , 'spice_instagram_register' ));
      add_action( 'admin_enqueue_scripts', array( $this,'spice_instagram_load_admin_script'));
      add_action( 'wp_enqueue_scripts', array( $this,'spice_instagram_load_script'));
      add_action( 'add_meta_boxes', array( $this, 'spice_instagram_meta_fn'));
      add_action( 'save_post',array( $this, 'spice_instagram_meta_save'));
      add_action( 'admin_menu',array( $this, 'spice_instagram_submenu'));
      add_action( 'admin_init',array( $this, 'spice_instagram_register_setting'));
    }

    //Register spice_instagram CPT callback function
    function spice_instagram_register() {
      $labels = array(
        'name'               => esc_html__( 'Instagram', 'spice-instagram' ),
        'singular_name'      => esc_html__( 'Instagram', 'spice-instagram' ),
        'add_new'            => esc_html__( 'Add New','spice-instagram' ),
        'add_new_item'       => esc_html__( 'Instagram Feeds', 'spice-instagram' ),
        'edit_item'          => esc_html__( 'Instagram', 'spice-instagram' ),
        'new_item'           => esc_html__( 'Instagram', 'spice-instagram' ),
        'all_items'          => esc_html__( 'All Instagram', 'spice-instagram' ),
        'view_item'          => esc_html__( 'View Instagram', 'spice-instagram' ),
        'search_items'       => esc_html__( 'Search Instagram', 'spice-instagram' ),
        'not_found'          => esc_html__( 'No instagram feeds found', 'spice-instagram' ),
        'not_found_in_trash' => esc_html__( 'No instagram feeds found in the Trash', 'spice-instagram' ), 
        'parent_item_colon'  =>  '',
        'menu_name'          => esc_html__( 'Instagram', 'spice-instagram' )
      );
      $args = array(
        'labels'        => $labels,
        'public'        => true,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-instagram',
        'supports'      => array( 'title'),
        'has_archive'   => false,
      );
    register_post_type( 'spice_instagram', $args ); 
    }

    //Submenu Callback callback function
    function spice_instagram_submenu()
    {
     add_submenu_page(
                     'edit.php?post_type=spice_instagram',
                     esc_html__( 'Spice Instagram Token', 'spice-instagram' ),
                     'Settings',
                     'manage_options',
                     'spice_insta_token',
                     array( $this, 'spice_instagram_render_page' ),);
    }

    //Function callback
    function spice_instagram_render_page()
    {
      echo '<div class="wrap">
      <h1>'.esc_html__( 'Instagram Settings', 'spice-instagram' ).'</h1>
      <div class="spice-instagram-wrapper">
        <p>'.esc_html__( 'Tutorial to get your Access Token', 'spice-instagram' ).' <a href="#" target="_blank">'.esc_html__( 'Click Here', 'spice-instagram' ).'</a> </p>
        <form method="post" action="options.php">';
          settings_fields( 'spice_instagram_settings' ); 
          do_settings_sections( 'spice_instagram' );
          submit_button();
          echo '</form>
      </div></div>';
    }


    //Register Form Field
    function spice_instagram_register_setting()
    {
      register_setting(
        'spice_instagram_settings',
        'spice_instagram_text',
        'sanitize_text_field'
      );

      add_settings_section(
        'some_settings_section_id',
        '',
        '',
        'spice_instagram'
      );

      add_settings_field(
        'spice_instagram_text',
        'Access Token',
        array( $this, 'spice_instagram_text_field_html' ),
        'spice_instagram',
        'some_settings_section_id',
        array( 
          'label_for' => 'spice_instagram_text',
          'class' => 'spice_instagram-class',
        )
      );

    }


    //Text Field Callback
    function spice_instagram_text_field_html()
    {

      $text = get_option( 'spice_instagram_text' );

      printf(
        '<input type="text" id="spice_instagram_text" name="spice_instagram_text" value="%s" />',
        esc_attr( $text )
      );

    }

    /**
     * Load Admin Script
     *
     * @since 1.0
     */
     public function spice_instagram_load_admin_script()
     {
      wp_enqueue_style( 'wp-color-picker' );
      wp_enqueue_script('spice-instagram-color-pic', SPICE_INSTAGRAM_URL.'assets/js/color-picker.js', array( 'wp-color-picker' ), false, true );
      wp_enqueue_style( 'spice-instagram-style', SPICE_INSTAGRAM_URL.'assets/css/style.css');
      wp_enqueue_style( 'spice-instagram-jquery-ui', SPICE_INSTAGRAM_URL.'assets/css/jquery-ui.css'); 
      wp_enqueue_script('jquery-ui-tabs');
      wp_enqueue_script('jquery-ui-slider');
      wp_enqueue_script('spice-instagram-custom-tabs', SPICE_INSTAGRAM_URL . 'assets/js/custom.js', array('jquery'));
     }

     /**
     * Load Front Script
     *
     * @since 1.0
     */
     public function spice_instagram_load_script()
     {
      wp_enqueue_style('spice-instagram-front',  SPICE_INSTAGRAM_URL . 'assets/css/front.css');
      // if access token value blank
      if(!empty(get_option('spice_instagram_text'))):
      wp_enqueue_style('spice-magnific-pop-up',  SPICE_INSTAGRAM_URL . 'assets/css/magnific-popup.css');  
      wp_enqueue_script('spice-instagram-feed',  SPICE_INSTAGRAM_URL . 'assets/js/nstafeed.min.js', array('jquery'), false, true );
      wp_enqueue_script('spice-magnific-pop-up', SPICE_INSTAGRAM_URL . 'assets/js/magnific-popup.js', array('jquery'), false, true );
      endif;
     }


    //Add Meta Box
    function spice_instagram_meta_fn()
    {
      add_meta_box( 'spice_instagram_meta_id', esc_html__('Spice Instagram Settings','spice-instagram'), array($this,'spice_instagram_meta_cb_fn'), 'spice_instagram','normal','high' );
      add_meta_box( 'spice_instagram_generator_id', esc_html__('Spice Instagram  Shortcode','spice-instagram'), array($this,'spice_instagram_generator_fn'), 'spice_instagram','side','low' );
    }


    //Callback Meta Function
    function spice_instagram_meta_cb_fn()
    {
      require_once('spice-instagram-meta-box.php');
    }

    //Shortcode Generator Function
    function spice_instagram_generator_fn()
    {
      require_once('spice-instagram-shortcode-generator.php');
    }


    //Save Meta Values
    function spice_instagram_meta_save($post_id) 
      {
        if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
              return;
          
        if ( ! current_user_can( 'edit_page', $post_id ) )
        {   return ;  } 
          
        if(isset( $_POST['post_ID']))
        {
          $post_ID = absint($_POST['post_ID']);
          $post_type=get_post_type($post_ID);
          
          if($post_type=='spice_instagram')
          {
            update_post_meta($post_ID, 'spice_instagram_padding_top', absint($_POST['spice_instagram_padding_top']));
            update_post_meta($post_ID, 'spice_instagram_padding_right', absint($_POST['spice_instagram_padding_right']));
            update_post_meta($post_ID, 'spice_instagram_padding_bottom', absint($_POST['spice_instagram_padding_bottom']));
            update_post_meta($post_ID, 'spice_instagram_padding_left', absint($_POST['spice_instagram_padding_left']));
            update_post_meta($post_ID, 'spice_instagram_image', absint($_POST['spice_instagram_image']));
            update_post_meta($post_ID, 'spice_instagram_col', absint($_POST['spice_instagram_col']));
            update_post_meta($post_ID, 'spice_instagram_overlay', sanitize_text_field($_POST['spice_instagram_overlay']));
            update_post_meta($post_ID, 'spice_instagram_title_align', sanitize_text_field($_POST['spice_instagram_title_align']));
            if(empty($_POST['spice_instagram_title']))
            {
              $spice_instagram_title='spice_instagram_blank';
            }
            else
            {
              $spice_instagram_title=sanitize_text_field($_POST['spice_instagram_title']);
            }
            update_post_meta($post_ID, 'spice_instagram_title', $spice_instagram_title);
            update_post_meta($post_ID, 'spice_instagram_title_ff', sanitize_text_field($_POST['spice_instagram_title_ff']));
            update_post_meta($post_ID, 'spice_instagram_title_fs', absint($_POST['spice_instagram_title_fs']));
            update_post_meta($post_ID, 'spice_instagram_title_lheight', absint($_POST['spice_instagram_title_lheight']));
            update_post_meta($post_ID, 'spice_instagram_title_fw', absint($_POST['spice_instagram_title_fw']));
            update_post_meta($post_ID, 'spice_instagram_title_fstyle', sanitize_text_field($_POST['spice_instagram_title_fstyle']));
            update_post_meta($post_ID, 'spice_instagram_title_trans', sanitize_text_field($_POST['spice_instagram_title_trans']));
            update_post_meta($post_ID, 'spice_instagram_title_clr', sanitize_hex_color($_POST['spice_instagram_title_clr']));
            update_post_meta($post_ID, 'spice_instagram_overlay_clr', sanitize_hex_color($_POST['spice_instagram_overlay_clr']));       
          }
        }       
      }


  }

}

new Spice_Instagram_Type();